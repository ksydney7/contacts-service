# README #

Sample RESTful Contact list API performing create, read, update and delete operations using Spring boot and JPA ORM layer to interact with MySQL RDBMS.
 
### DATABASE SCHEMA
    -- -----------------------------------------------------
    -- Create Schema `contacts-service-db`
    -- -----------------------------------------------------
    CREATE SCHEMA IF NOT EXISTS `contacts-service-db` DEFAULT CHARACTER SET utf8 ;
    
    -- -----------------------------------------------------
    -- USE Schema `contacts-service-db`
    -- -----------------------------------------------------
    USE `contacts-service-db` ;
    
    -- -----------------------------------------------------
    -- Table `contacts`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `contacts` ;
    CREATE TABLE IF NOT EXISTS `contacts` (
      `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
      `first_name` VARCHAR(100) NOT NULL,
      `last_name` VARCHAR(100) NOT NULL,
      `email_address` VARCHAR(50) NOT NULL,
      `mobile_number` VARCHAR(100) NOT NULL,
      `fax_number` VARCHAR(255) DEFAULT NULL,
      `tel_number` VARCHAR(255) DEFAULT NULL,
      PRIMARY KEY (`id`))
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;
    
    -- -----------------------------------------------------
    -- Table `contacts` Index
    -- -----------------------------------------------------
    CREATE UNIQUE INDEX `mobile_number_index` ON `contacts` (`mobile_number`);
    CREATE UNIQUE INDEX `email_address_index` ON `contacts` (`email_address`);
 
### Running using maven
    
* Run the spring boot as below with local MySQL instance
    * mvn -DDB_USERNAME=REPLACE_WITH_YOUR_USERNAME -DDB_USERNAME=REPLACE_WITH_YOUR_PASSWORD spring-boot:run

* Run the spring boot as below with remote MySQL instance
    * mvn -DDB_HOST=REPLACE_WITH_YOUR_HOST -DDB_USERNAME=REPLACE_WITH_YOUR_USERNAME -DDB_USERNAME=REPLACE_WITH_YOUR_PASSWORD spring-boot:run
    
* Run the spring boot integration test as below 
    * mvn clean install

### Endpoints on localhost
* Get All Contacts (HTTP METHOD GET) - http://localhost:8080/contacts 
* Get Contact (HTTP METHOD GET) - http://localhost:8080/contacts/{id} 
* Delete Contact(HTTP METHOD DELETE)  - http://localhost:8080/contacts/{id}
* Create Contact (HTTP METHOD POST) - http://localhost:8080/contacts 
* Update Contact (HTTP METHOD PUT) - http://localhost:8080/contacts/{id}      
 
### Create & Update Contact JSON OBJECT ###
    {
    "firstName": "",
    "lastName": "",
    "emailAddress": "",
    "mobileNumber": "",
    "faxNumber": "",
    "telNumber": ""
    }        
### Who do I talk to? ###

* Kgothatso Mathabatha ksydney7@gmail.com