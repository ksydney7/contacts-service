package za.co.dvt.contacts.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApiFieldError {

    private String message;

    private Date timestamp;

    private List<ApiSubError> errors = new ArrayList<>();

    public ApiFieldError(String message, Date timestamp, List<ApiSubError> errors) {
        this.message = message;
        this.timestamp = timestamp;
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<ApiSubError> getErrors() {
        return errors;
    }

    public void setErrors(List<ApiSubError> errors) {
        this.errors = errors;
    }
}
