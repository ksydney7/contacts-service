package za.co.dvt.contacts.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.dvt.contacts.models.Contact;

@Repository(value = "contactRepository")
public interface ContactRepository extends CrudRepository<Contact, Long> {

    long countByEmailAddress(String emailAddress);

    long countByMobileNumber(String mobileNumber);
}
