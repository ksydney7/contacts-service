package za.co.dvt.contacts.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import za.co.dvt.contacts.models.Contact;
import za.co.dvt.contacts.repositories.ContactRepository;

@Component(value = "contactValidator")
public class ContactValidator implements Validator {

    @Autowired
    @Qualifier(value = "contactRepository")
    private ContactRepository contactRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return Contact.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Contact contact = (Contact) target;

        if (contactRepository.countByEmailAddress(contact.getEmailAddress()) != 0)
            errors.rejectValue("emailAddress", null, "E-Mail Address Already Exists");

        if (contactRepository.countByMobileNumber(contact.getMobileNumber()) != 0)
            errors.rejectValue("mobileNumber", null, "Mobile Number Already Exists");
    }
}
