package za.co.dvt.contacts.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity
@Table(name = "contacts")
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotEmpty(message = "First Name is required")
    @Column(name = "first_name", nullable = false, length = 100)
    private String firstName;

    @NotEmpty(message = "Last Name is required")
    @Column(name = "last_name", nullable = false, length = 100)
    private String lastName;

    @NotEmpty(message = "E-Mail Address is required")
    @Email(message = "E-Mail Address is invalid")
    @Column(name = "email_address", nullable = false, length = 50)
    private String emailAddress;

    @NotEmpty(message = "Mobile Number is required")
    @Column(name = "mobile_number", nullable = false, length = 20)
    private String mobileNumber;

    @Column(name = "fax_number", length = 20)
    private String faxNumber;

    @Column(name = "tel_number", length = 20)
    private String telNumber;

    public Contact() {
        super();
    }

    public Contact(@NotEmpty(message = "First Name is required") String firstName, @NotEmpty(message = "Last Name is required") String lastName,
                   @NotEmpty(message = "E-Mail Address is required") @Email(message = "E-Mail Address is invalid") String emailAddress,
                   @NotEmpty(message = "Mobile Number is required") String mobileNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
    }

    public Contact(@NotEmpty(message = "First Name is required") String firstName, @NotEmpty(message = "Last Name is required") String lastName,
                   @NotEmpty(message = "E-Mail Address is required") @Email(message = "E-Mail Address is invalid") String emailAddress,
                   @NotEmpty(message = "Mobile Number is required") String mobileNumber, String faxNumber, String telNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
        this.faxNumber = faxNumber;
        this.telNumber = telNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }


    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }


    @Override
    public String toString() {

        StringBuffer sb = new StringBuffer();
        sb.append("Id: ").append(this.id);
        sb.append(", First Name: ").append(this.firstName);
        sb.append(", Last Name: ").append(this.lastName);
        sb.append(", E-Mail Address: ").append(this.emailAddress);
        sb.append(", Mobile Number: ").append(this.mobileNumber);
        sb.append(", Fax Number: ").append(this.faxNumber);
        sb.append(", Tel Number: ").append(this.telNumber);
        return sb.toString();
    }

}
