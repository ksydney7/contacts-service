package za.co.dvt.contacts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import za.co.dvt.contacts.dto.ApiFieldError;
import za.co.dvt.contacts.dto.ApiSubError;
import za.co.dvt.contacts.models.Contact;
import za.co.dvt.contacts.repositories.ContactRepository;
import za.co.dvt.contacts.validator.ContactValidator;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/contacts", produces = "application/json")
public class ContactController {

    @Autowired
    @Qualifier(value = "contactValidator")
    private ContactValidator contactValidator;

    @Autowired
    @Qualifier(value = "contactRepository")
    private ContactRepository contactRepository;

    /**
     * Obtains the list of all contacts currently created in the system.
     * If the list is successfully obtained, the list of existing contacts is returned, along with an HTTP 200(OK) status.
     *
     * @return Iterable<Contact>
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Contact>> findAllContacts() {

        Iterable<Contact> contacts = contactRepository.findAll();

        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }

    /**
     * Creates a new contact.
     * This request should carry a request body that includes the data that should be associated with the newly created contact.
     * If the contact is created, an HTTP 201 (Created) status is returned along with the newly created contact in the response body.
     *
     * @param contact
     * @param bindingResult
     * @return Object
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> createContact(@Valid @RequestBody Contact contact, BindingResult bindingResult) {

        contactValidator.validate(contact, bindingResult);

        if (!bindingResult.hasErrors()) {

            contact = contactRepository.save(contact);

            return new ResponseEntity<>(contact, HttpStatus.CREATED);
        }

        List<ApiSubError> apiFieldErrors = bindingResult
                .getFieldErrors()
                .stream()
                .map(fieldError -> new ApiSubError(
                        fieldError.getField(),
                        fieldError.getRejectedValue(),
                        fieldError.getDefaultMessage()))
                .collect(toList());

        ApiFieldError apiFieldError = new ApiFieldError("Validation Failed", new Date(), apiFieldErrors);

        return new ResponseEntity<>(apiFieldError, HttpStatus.BAD_REQUEST);
    }

    /**
     * Obtains the contact associated with the given ID. If no contact exists, an HTTP 404 (Not Found) status is returned.
     * If the contact can be found, an HTTP 200 status is returned and the response body contains the information associated with the contact.
     *
     * @param id
     * @return Contact
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Contact> findContactById(@PathVariable long id) {

        Optional<Contact> contactOptional = contactRepository.findById(id);

        if (!contactOptional.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(contactOptional.get(), HttpStatus.OK);
    }

    /**
     * Updates an existing contact. If no contact with the given ID can be found, an HTTP 404 status is returned.
     * If an contact exists with the given ID and the request body contains valid updates to the contact,
     * the contact is updated and the updated contact is returned in the response body, along with an HTTP 200 status.
     *
     * @param contact
     * @param id
     * @return Contact
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<Contact> updateContact(@Valid @RequestBody Contact contact, @PathVariable long id) {

        Optional<Contact> contactOptional = contactRepository.findById(id);

        if (!contactOptional.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        contact.setId(id);

        contact = contactRepository.save(contact);

        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    /**
     * Deletes an contact with the given ID. If no contact exists, an HTTP 404 (Not Found) status is returned.
     * If the contact exists, it is deleted, and an HTTP 410 (Gone) status is returned.
     *
     * @param id
     * @return Void
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteContactById(@PathVariable long id) {

        Optional<Contact> contactOptional = contactRepository.findById(id);

        if (!contactOptional.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        contactRepository.delete(contactOptional.get());

        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
