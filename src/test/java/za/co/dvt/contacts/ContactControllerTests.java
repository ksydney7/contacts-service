package za.co.dvt.contacts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.dvt.contacts.models.Contact;

import java.util.List;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@ActiveProfiles(value = "test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
@Sql({"classpath:data.sql"})
public class ContactControllerTests {

    final static Logger logger = LoggerFactory.getLogger(ContactControllerTests.class);

    private static final String URL = "/contacts/";

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * test create contact and ensure correct response
     *
     * @throws Exception
     */
    @Test
    public void testCreateContactEnsureCorrectResponse() throws Exception {

        Contact contact = new Contact("Justine", "Henderson", "Justine.Henderson@jhb.dvt.co.za",
                "+27 78 759 5930", "+27 86 650 7203", "+27 11 759 5930");

        ResponseEntity<Contact> responseEntity = restTemplate.postForEntity(URL, contact, Contact.class);

        Contact resultContact = responseEntity.getBody();

        logger.info("Result Contact:: " + resultContact);

        assertEquals("Incorrect Response Status", HttpStatus.CREATED, responseEntity.getStatusCode());

        assertNotNull(resultContact);

        assertNotNull(resultContact.getId().longValue());
    }

    /**
     * test get contact and ensure correct response
     *
     * @throws Exception
     */
    @Test
    public void testGetContactEnsureCorrectResponse() throws Exception {

        ResponseEntity<Contact> responseEntity = restTemplate.getForEntity(URL + "{id}", Contact.class, new Long(1));

        Contact resultContact = responseEntity.getBody();

        logger.info("Result Contact:: " + resultContact);

        assertEquals("Incorrect Response Status", HttpStatus.OK, responseEntity.getStatusCode());

        assertNotNull(resultContact);

        assertEquals(1l, resultContact.getId().longValue());

    }

    /**
     * test get contact not exist and ensure correct response
     *
     * @throws Exception
     */
    @Test
    public void testGetContactNotExistEnsureCorrectResponse() throws Exception {

        ResponseEntity<Contact> responseEntity = restTemplate.getForEntity(URL + "{id}", Contact.class, new Long(100));

        Contact resultContact = responseEntity.getBody();

        assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        assertNull(resultContact);
    }

    /**
     * test get all contacts and ensure correct response
     *
     * @throws Exception
     */
    @Test
    public void testGetAllContactEnsureCorrectResponse() throws Exception {

        ResponseEntity<List> responseEntity = restTemplate.getForEntity(URL, List.class);

        int status = responseEntity.getStatusCodeValue();

        List<Contact> empListResult = null;

        if (responseEntity.getBody() != null) {
            empListResult = responseEntity.getBody();
        }

        assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);

        assertNotNull("Contacts not found", empListResult);

        assertEquals("Incorrect Contact List", 1, empListResult.size());
    }


    /**
     * test delete contact and ensure correct response
     *
     * @throws Exception
     */
    @Test
    public void testDeleteContactEnsureCorrectResponse() throws Exception {

        ResponseEntity<Void> responseEntity = restTemplate.exchange(URL + "{id}", HttpMethod.DELETE,
                null, Void.class, new Long(1));

        assertEquals("Incorrect Response Status", HttpStatus.GONE, responseEntity.getStatusCode());

    }

    /**
     * test update contact and ensure correct response
     *
     * @throws Exception
     */
    @Test
    public void testUpdateContactEnsureCorrectResponse() throws Exception {

        Contact contact = new Contact("Waylon", "Dalton", "Waylon.Dalton@gmail.com",
                "0725459036");

        logger.info("Contact:: " + contact);

        HttpEntity<Contact> requestEntity = new HttpEntity<>(contact);

        ResponseEntity<Contact> responseEntity = restTemplate.exchange(URL + "{id}", HttpMethod.PUT, requestEntity, Contact.class, new Long(1));

        Contact resultContact = responseEntity.getBody();

        logger.info("Result Contact:: " + resultContact);

        assertEquals("Incorrect Response Status", HttpStatus.OK, responseEntity.getStatusCode());

        assertNull("Contacts Fax Number Not Null", resultContact.getFaxNumber());

        assertNull("Contacts Tel Number Not Null", resultContact.getTelNumber());
    }
}
