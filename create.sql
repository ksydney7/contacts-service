
-- -----------------------------------------------------
-- Create Schema `contacts-service-db`
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `contacts-service-db` DEFAULT CHARACTER SET utf8 ;

-- -----------------------------------------------------
-- USE Schema `contacts-service-db`
-- -----------------------------------------------------
USE `contacts-service-db` ;

-- -----------------------------------------------------
-- Table `contacts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contacts` ;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `email_address` VARCHAR(50) NOT NULL,
  `mobile_number` VARCHAR(100) NOT NULL,
  `fax_number` VARCHAR(255) DEFAULT NULL,
  `tel_number` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `contacts` Index
-- -----------------------------------------------------
CREATE UNIQUE INDEX `mobile_number_index` ON `contacts` (`mobile_number`);
CREATE UNIQUE INDEX `email_address_index` ON `contacts` (`email_address`);